package com.app.webox.services;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@Slf4j
class YoutubeServiceTest {

    @Autowired
    YoutubeService youtubeService;

    @Test
    void shouldCheckYoutubeSearchResult(){
        log.info("Youtube results --> {}", youtubeService.searchForVideos("black widow"));
        assertThat(youtubeService.searchForVideos("black widow")).isNotNull();
        assertThat(youtubeService.searchForVideos("black widow").size()).isEqualTo(30);
    }

}