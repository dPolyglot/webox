package com.app.webox.controller;

import com.app.webox.model.Video;
import com.app.webox.dto.VideoDto;
import com.app.webox.services.YoutubeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
@Slf4j
public class WeboxController {
    @Autowired
    private YoutubeService youtubeService;

    @GetMapping("")
    public String youtubeVideos(Model model){

        model.addAttribute("searchQuery", new VideoDto());

        return "index";
    }

    @PostMapping("")
    public String submitSearch(@ModelAttribute @Valid VideoDto videoDto, BindingResult bindingResult, Model model){

        if(bindingResult.hasErrors()){
            return "index";
        }

        List<Video> videos = youtubeService.searchForVideos(videoDto.getSearchQuery());

        if (videos != null && videos.size() > 0) {
            model.addAttribute("videoResults", videos.size());
        } else {
            model.addAttribute("videoResults", 0);
        }

        model.addAttribute("videos", videos);

        model.addAttribute("youtubeVideoCriteria", videoDto);

        return "redirect:/index";
    }
}
