package com.app.webox.model;

import lombok.Data;

@Data
public class Video {
    private String title;
    private String url;
    private String thumbnailUrl;
    private String publishDate;
    private String description;
}
