## Webox

## Description
A movie search application

## Technologies used
SpringMVC, Youtube API

## Features
* Registration
* Login
* View videos
* Play a video

## How to use
www.herokuapp.com/webox
